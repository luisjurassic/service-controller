﻿using Service.Controller.Enums;
using System;
using System.Diagnostics;
using System.IO;
using System.ServiceProcess;

namespace Service.Controller
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Service Controller";
            Console.WriteLine($"Service Controller v{typeof(Program).Assembly.GetName().Version}");
            try
            {
                InformacaoServico info = VerificarArgumentos(args);
                ExecutarAcao(info);
                Console.WriteLine("Ação concluída com sucesso!");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private static void ExecutarAcao(InformacaoServico info)
        {
            if (info.Acao == Acao.Desinstalar)
            {
                if (!string.IsNullOrWhiteSpace(info.Caminho))
                    InstalarServico(info.Caminho, true);
                RemoverServico(info.Nome);
                return;
            }

            if (info.Acao == Acao.Instalar || info.Acao == Acao.InstalarEIniciar)
            {
                InstalarServico(info.Caminho);
                if (info.Acao == Acao.Instalar)
                    return;
            }

            var servico = new System.ServiceProcess.ServiceController(info.Nome);
            TimeSpan timeout = TimeSpan.FromMilliseconds(info.Timeout);

            switch (info.Acao)
            {
                case Acao.Iniciar:
                case Acao.InstalarEIniciar:
                    IniciarServico(servico, timeout);
                    break;
                case Acao.Parar:
                    PararServico(servico, timeout);
                    break;
                default:
                    throw new NotImplementedException("Ação ainda não implementada.");
            }
        }

        private static void InstalarServico(string arquivo, bool desinstalar = false)
        {
            bool is64 = BuscarArquiteturaProcessador() == ArquiteturaProcessador.x64;
            string args = $"/c \"%windir%\\Microsoft.NET\\Framework{(is64 ? "64" : "")}\\v4.0.30319\\installutil {(desinstalar ? "-u " : "")}\"{arquivo}\"\"";
            Process.Start(new ProcessStartInfo("cmd.exe", args)).WaitForExit();
        }

        private static void RemoverServico(string nomeServico)
        {
            string args = $"/c \"SC STOP \"{nomeServico}\" & SC DELETE \"{nomeServico}\"\"";
            Process.Start(new ProcessStartInfo("cmd.exe", args)).WaitForExit();
        }

        private static ArquiteturaProcessador BuscarArquiteturaProcessador()
        {
            try
            {
                string arq = Environment.GetEnvironmentVariable("PROCESSOR_ARCHITECTURE", EnvironmentVariableTarget.Machine);

                if (arq == "AMD64" || arq == "IA64")
                    return ArquiteturaProcessador.x64;
                else
                    return ArquiteturaProcessador.x86;
            }
            catch
            {
                return Environment.Is64BitOperatingSystem ? ArquiteturaProcessador.x64 : ArquiteturaProcessador.x86;
            }
        }

        private static void IniciarServico(ServiceController servico, TimeSpan timeout)
        {
            servico.Start();
            servico.WaitForStatus(ServiceControllerStatus.Running, timeout);
        }

        private static void PararServico(ServiceController servico, TimeSpan timeout)
        {
            servico.Stop();
            servico.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
        }

        private static InformacaoServico VerificarArgumentos(string[] args)
        {
            if (args.Length == 0)
                throw new Exception("Nenhum serviço informado.\nUse -help para ver os comandos disponíveis.");

            string servico = null,
                argument = null,
                caminho = null;
            Acao? comando = null;
            int timeout = 30000;

            for (int i = 0; i < args.Length; i++)
            {
                argument = args[i].Trim().TrimStart('-');

                if (args[i].StartsWith("-"))
                {
                    string[] argumentSplit = argument.ToLower().Split('=');
                    argument = argumentSplit[0];

                    switch (argument)
                    {
                        case "help":
                        case "?":
                            Console.WriteLine("--------------- Service Controller ---------------");
                            Console.WriteLine("\r-help -?\t\t para ajuda.");
                            Console.WriteLine("");
                            Console.WriteLine("<nome do serviço>\t\t\t(OBRIGATÓRIO) Nome do serviço de preferência com aspas duplas.");
                            Console.WriteLine("-iniciar\t\t\t\tInicia o serviço informado.");
                            Console.WriteLine("-InstalarEIniciar=<caminho do serviço>\tInstala o serviço na máquina atual e inicia o serviço informado.");
                            Console.WriteLine("-instalar=<caminho do serviço>\t\tInstala o serviço na máquina atual.");
                            Console.WriteLine("-desinstalar=<caminho do serviço>\tDesinstala o serviço na máquina atual.");
                            Console.WriteLine("-parar\t\t\t\t\tPara o serviço informado.");
                            Console.WriteLine("-t=<int> ou -tempo=<int>\t\tTempo de espera para a mudança de status do serviço.");
                            throw new Exception("------------------------------------------------------");
                        case "iniciar":
                            comando = Acao.Iniciar;
                            break;
                        case "instalareiniciar":
                            caminho = ValidarCaminho(argumentSplit);
                            comando = Acao.InstalarEIniciar;
                            break;
                        case "instalar":
                            caminho = ValidarCaminho(argumentSplit);
                            comando = Acao.Instalar;
                            break;
                        case "desinstalar":
                            try
                            {
                                caminho = ValidarCaminho(argumentSplit);
                            }
                            catch (ArgumentException)
                            {
                            }
                            comando = Acao.Desinstalar;
                            break;
                        case "parar":
                            comando = Acao.Parar;
                            break;
                        case "t":
                        case "tempo":
                            timeout = ValidarTimeout(argumentSplit);
                            break;
                        default:
                            throw new Exception($"\rO comando \"{args[i].TrimStart('-')}\" não pode ser encontrado.\n\rUse -help para ajuda.");
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(servico))
                        servico = argument;
                    else
                        throw new Exception($"Comandos devem ter \"-\" na frente.\nO argumento \"{args[i]}\" é inválido.\nUse -help para ajuda.");
                }
            }

            if (!comando.HasValue)
                throw new Exception("Nenhum comando informado.\n\rUse -help para ajuda.");

            if (comando != Acao.Instalar && string.IsNullOrWhiteSpace(servico))
                throw new Exception("É obrigatório informar o nome do serviço.\n\rUse -help para ajuda.");

            return new InformacaoServico(comando.Value, caminho, servico, timeout);
        }

        private static int ValidarTimeout(string[] argumentSplit)
        {
            if (argumentSplit.Length < 2)
                throw new Exception("Tempo de espera não informado!");

            int tempo;
            if (int.TryParse(argumentSplit[1], out tempo))
                return tempo;
            else
                throw new Exception("Tempo informado não é um inteiro válido.");
        }

        private static string ValidarCaminho(string[] argumentSplit)
        {
            if (argumentSplit.Length < 2)
                throw new ArgumentException("Caminho do arquivo não informado!");

            if (!File.Exists(argumentSplit[1]))
                throw new IOException("Arquivo informado não existe!");

            return argumentSplit[1];
        }
    }
}
