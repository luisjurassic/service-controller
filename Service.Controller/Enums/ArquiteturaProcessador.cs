﻿namespace Service.Controller.Enums
{
    enum ArquiteturaProcessador : int
    {
        x86 = 0,
        x64 = 1,
    }
}