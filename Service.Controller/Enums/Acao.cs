﻿namespace Service.Controller.Enums
{
    enum Acao
    {
        Iniciar,
        Instalar,
        Desinstalar,
        InstalarEIniciar,
        Parar
    }
}