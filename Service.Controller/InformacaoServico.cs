﻿using Service.Controller.Enums;

namespace Service.Controller
{
    class InformacaoServico
    {
        public Acao Acao { get; set; }

        public string Caminho { get; set; }

        public string Nome { get; set; }

        public int Timeout { get; set; }

        public InformacaoServico(Acao acao, string caminho, string nome, int timeout)
        {
            Acao = acao;
            Caminho = caminho;
            Nome = nome;
            Timeout = timeout;
        }
    }
}
